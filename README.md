# FameRPG
A fully featured RPG centered around the culture of Fame, shipped as a bot for Discord

![The Ultimate Map](/assets/map/fullmap.png)

# License
FameRPG is libre software licensed under the GNU GPLv3 license. For more information please read the LICENSE file.
